import { Component, OnInit } from '@angular/core';
import { EmployeeDataService } from '../employee-data.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss']
})
export class EmployeeAddComponent implements OnInit {
  title = "New Employee";
  emData : any = {
    id:'', name: '', department: ''
  }
  constructor(
    private emSvr: EmployeeDataService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit(){
    this.emSvr.postEmployee(this.emData)
      .subscribe(
        (doc) => {
          if(doc['code'] == 200){
            alert(doc['message'])
            this.router.navigate(['/employees']);
          }else{
            alert('Data not saved!')
          }
        },
        (err) => {
          console.log(err)
          if(err.status == 0){
            alert('No connection to server.')
          }else{
            alert(err.error.message)
          }
        }
      )
  }
}
