import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeDataService } from '../employee-data.service'

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss']
})
export class EmployeeEditComponent implements OnInit {
  title = 'Edit Employee';
  emId: any = '';
  emDataNow: any = {}

  constructor(
    private actRoute: ActivatedRoute,
    private emSvr: EmployeeDataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getParam();
  }

  getParam(){
    this.actRoute.params.subscribe((param) => {
      this.emId = param['id'];
      this.onGetById();
    })
  }

  onGetById(){
    this.emSvr.getEmployeeById(this.emId)
      .subscribe(
        (doc) => {
          this.emDataNow = doc['data'][0]
        },
        (err) => {
          console.log(err)
        }
      )
  }

  onSave(){
    //THE this.emDataNow CAN BE EDITED AND WE CAN USE IT TO UPDATE THE DATE IN DATABASE
    //SO, THIS onSave METHOD IS A PREAPARATION FOR THAT TASK
    /* console.log(this.emDataNow) */

    this.emSvr.putEmployee(this.emDataNow)
      .subscribe(
        (doc) => {
          if(doc['code'] == 200){
            alert(doc['message'])
            this.router.navigate(['/employees']);
          }else{
            alert('Data not saved!')
          }
        },
        (err) => {
          console.log(err)
          if(err.status == 0){
            alert('No connection to server.')
          }else{
            alert(err.error.message)
          }
        }
      )
  }

  onDelete(){
    //THE this.emDataNow.id CAN WE USE AS PARAM FOR DELETE THE DATE IN DATABASE
    //SO, THIS onDelete METHOD IS A PREAPARATION FOR THAT TASK
    /* console.log(this.emDataNow.id) */

    this.emSvr.delEmployee(this.emDataNow.id)
      .subscribe(
        (doc) => {
          if(doc['code'] == 200){
            alert(doc['message'])
            this.router.navigate(['/employees']);
          }else{
            alert('Data not saved!')
          }
        },
        (err) => {
          console.log(err)
          if(err.status == 0){
            alert('No connection to server.')
          }else{
            alert(err.error.message)
          }
        }
      )
  }
}
