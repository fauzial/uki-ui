import { Component, OnInit } from '@angular/core';
import { EmployeeDataService } from '../employee-data.service'

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  title = 'Employee List';
  employees: any = [];

  constructor(private emSrv: EmployeeDataService) { }

  ngOnInit() {
    this.onGetAllEmployee()
  }

  onGetAllEmployee(){
    this.emSrv.getAllEmployee()
      .subscribe(
        (docs) => {
          this.employees = docs['data']
        },
        (err) => {
          console.log(err)
        }
      )
  }
}
